const expect = require("chai").expect;

/*Dependencies*/
const Luhn = require('../luhn');


/*Luhn Test Data*/
const validCard1 = "5454545454545454";
const validCard2 = "4532690815261968";
const inValidCard1 = "1234567812345678";
const inValidCard2 = "1234567890123456";
const inValidCard3 = "49927398717";

describe("Test Luhn validations of card", function() {

  it("Should validate card", function() {
    expect(Luhn(validCard1)).to.equal(true);
  });

  it("Should validate card", function() {
    expect(Luhn(validCard2)).to.equal(true);
  });

  it("Should Invalidate card", function() {
    expect(Luhn(inValidCard1)).to.equal(false);
  });

  it("Should Invalidate card", function() {
    expect(Luhn(inValidCard2)).to.equal(false);
  });

  it("Should Invalidate card", function() {
    expect(Luhn(inValidCard3)).to.equal(false);
  });

});