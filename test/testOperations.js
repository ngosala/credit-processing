const expect = require("chai").expect;

/*Dependencies*/
const Credit = require('../credit');
const cardOperations = require('../cardFunctions');

/*Data store to test*/
const allCards = {};

/*Test Data*/
const addValid = "Add Tom 4111111111111111 $1000";
const addInValid = "Add Dan 49927398717 $5000";
const chargeCard = "Charge Tom $565";
const chargeCardbalance = "Charge Tom $435";
const creditCard = "Credit Tom $400";
const creditCardNegBal = "Credit Tom $1000";
const chargeCardDan = "Charge Dan $565";
const creditCardDan = "Credit Dan $400";

describe("Test operations on card", function() {
  it("Should add card with valid details", function() {
    let details = addValid.split(' ');
    cardOperations.addCard(details, function(data) {
      allCards[details[1]] = new Credit(data);
      expect(allCards['Tom'].name).to.equal('Tom');
      expect(allCards['Tom'].number).to.equal('4111111111111111');
      expect(allCards['Tom'].limit).to.equal('1000');
      expect(allCards['Tom'].balance).to.equal(0);
      expect(allCards['Tom'].isValid).to.equal(true);
    });
  });

  it("Should increase balance of card", function() {
    let details = chargeCard.split(' ');
    cardOperations.chargeCard(allCards[details[1]], parseInt(details[2].substr(1)));
    expect(allCards['Tom'].balance).to.equal(565);
  });

  // As balance will be greater than credit limit
  it("Should ignore the charge transaction", function() {
    let details = chargeCard.split(' ');
    cardOperations.chargeCard(allCards[details[1]], parseInt(details[2].substr(1)));
    expect(allCards['Tom'].balance).to.equal(565);
  });

  it("Should complete the charge transaction", function() {
    let details = chargeCardbalance.split(' ');
    cardOperations.chargeCard(allCards[details[1]], parseInt(details[2].substr(1)));
    expect(allCards['Tom'].balance).to.equal(1000);
  });

  it("Should complete the credit transaction", function() {
    let details = creditCard.split(' ');
    cardOperations.creditCard(allCards[details[1]], parseInt(details[2].substr(1)));
    expect(allCards['Tom'].balance).to.equal(600);
  });

  it("Should complete the credit transaction with negative balance", function() {
    let details = creditCardNegBal.split(' ');
    cardOperations.creditCard(allCards[details[1]], parseInt(details[2].substr(1)));
    expect(allCards['Tom'].balance).to.equal(-400);
  });

  it("Should add card with Invalid details", function() {
    let details = addInValid.split(' ');
    cardOperations.addCard(details, function(data) {
      allCards[details[1]] = new Credit(data);
      expect(allCards['Dan'].name).to.equal('Dan');
      expect(allCards['Dan'].isValid).to.equal(false);
    });
  });

  it("Should ignore the Charge transaction", function() {
    let details = chargeCardDan.split(' ');
    cardOperations.chargeCard(allCards[details[1]], parseInt(details[2].substr(1)));
    expect(allCards['Dan'].balance).to.equal(0);
  });

  it("Should ignore the credit transaction", function() {
    let details = creditCardDan.split(' ');
    cardOperations.chargeCard(allCards[details[1]], parseInt(details[2].substr(1)));
    expect(allCards['Dan'].balance).to.equal(0);
  });
});