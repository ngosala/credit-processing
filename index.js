/* Import Section */
const Credit = require('./credit');
const cardOperations = require('./cardFunctions');
const readline = require('readline');
const fs = require('fs');


/* Setup to read from file or stdin */
/* Set encoding to utf8 */
process.stdin.setEncoding('utf8');

/*
 Datastore to hold data of all cards
Credit Data Structure
number  -> credit card number
name    -> name on credit card
limit   -> card limit
balance -> balance on card
isValid -> true if card is valid (luhn 10)
*/
let allCards = {};

/* Execute this for each transaction/ operation. Each operation is of type
Add or Charge or Credit */
const transaction = function(operation) {
  /*
  After split,
  details[0] -> operationType
  details[1] -> name
  details[2] -> if operation is add, this is card number
  details[2] -> for charge and credit, this is amount
  details[3] -> if operation is add, this is limit
  */
  let details = operation.split(' ');

  /*If operation is add, then add card after checking if the card is valid*/
  if (details[0] === "Add") {
    cardOperations.addCard(details, function(data) {
      allCards[details[1]] = new Credit(data);
    });
  }
  /*if operation is charge, check if credit card is valid and charge it, else, ignore the operation*/
  else if (details[0] === "Charge" && allCards[details[1]].isValid) {
    allCards[details[1]] = cardOperations.chargeCard(allCards[details[1]], parseInt(details[2].substr(1)));
  }
  /*if operation is credit, check if credit card is valid and credit it, else, ignore the operation*/
  else if (details[0] === "Credit" && allCards[details[1]].isValid) {
    allCards[details[1]] = cardOperations.creditCard(allCards[details[1]], parseInt(details[2].substr(1)));
  }
}

/*Process the input by line and return summary at the end*/
const processInput = function(rl) {
  /*Read by everyline and perform operation */
  rl.on('line', (operation) => {
    transaction(operation);
  });

  /* After reading the input, print the summary */
  rl.on('close', function() {
    cardOperations.summary(allCards);
  });
}

/*
check if stdin is tty. if not, read from stream,
else look for input as argument
*/
console.log("```");
console.log("Given Input");
console.log("```");
if (process.stdin.isTTY) {
  process.argv.forEach((val, index) => {
    /*
    index:0 for node exec
    index:1 for node script
    index:2 for argument passed
    */
    if (index === 2) {
      /* Create interface to read by line from file*/
      const rl = readline.createInterface({
        input: fs.createReadStream(val),
        output: process.stdout
      });
      processInput(rl);
    }
  });
} else {
  /* Create interface to read by line from stdin */
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  processInput(rl);
}