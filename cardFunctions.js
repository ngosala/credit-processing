/* Credit card functions
 Add: To add credit card after validating card with custom Luhn 10 validation
 Charge: Will increase the balance of valid credit cards
 Credit: will reduce the balance of valid credit cards
*/
/*Import Luhn function*/
const Luhn = require('./luhn');
/* Add card will validate credit card against Luhn check and add it to data store */
const addCard = function(details, callback) {
  /*
  Build Card data
  details[1] -> name
  details[2] -> if operation is add, this is card number
  details[3] -> if operation is add, this is limit
  */
  let cardData = {};
  if (Luhn(details[2])) {
    cardData = {
      name: details[1],
      number: details[2],
      limit: details[3].substr(1),
      isValid: true
    }
  } else {
    cardData = {
      name: details[1],
      number: details[2],
      limit: 0,
      isValid: false
    }
  }
  /*callback with built card data*/
  callback(cardData);

}

/*If amount is less than or equal to balance, increase balance, else ignore transaction*/
const chargeCard = function(cardData, amount) {
  if ((cardData.limit - cardData.balance) >= amount) {
    cardData.balance = cardData.balance + amount;
  }
  return cardData;
}

/*Reduce balance by specified amount*/
const creditCard = function(cardData, amount) {
  cardData.balance = cardData.balance - amount;
  return cardData;
}

/*print summary sort by name. If the card is not valid, report as error*/
const summary = function(allCards) {
  let names = Object.keys(allCards).sort();
  console.log("```\n");
  console.log("```");
  console.log("Summary");
  console.log("```");
  for (let i = 0; i < names.length; i++) {
    if (allCards[names[i]].isValid)
      console.log(names[i] + ": $" + allCards[names[i]].balance);
    else
      console.log(names[i] + ": error");
  }
  console.log("```");
}


module.exports = {
  addCard: addCard,
  chargeCard: chargeCard,
  creditCard: creditCard,
  summary: summary
}