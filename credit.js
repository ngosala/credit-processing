// Create class to hold Credit card data structure
module.exports = class Credit {
  constructor(card) {
    this.number = card.number;
    this.name = card.name;
    this.limit = card.limit;
    this.isValid = card.isValid;
    this.balance = 0;
  }
}