# credit processing

```````````````````
Instructions
```````````````````
1. Install the dependencies using npm command in the project folder
    `npm install`
2. Execute project using one of the below commands
    `node index.js input.txt`
      or
    `node index.js < input.txt`
3. Tests can be executed using the below commands
    `npm test`
      or
    `./node_modules/.bin/mocha test/`

```````````````````
Overview
```````````````````
1. Credit class holds the data structure
2. Asynchronous design pattern is used with callback
3. The whole project is architected in a modular way
4. Each read stream has a output stream pointing to process.stdout which prints to the output
5. Every module has unit tests associated with it

```````````````````
Why NodeJS?
```````````````````
1. With the single main threaded event loop, it makes easy to execute multiple instructions concurrently
2. It's easy to implement async design patterns
3. With Mocha and Chai, it's easier to write unit tests
3. NodeJS has a huge advantage of speed when doing IO operations or
   concurrent operations with the help of chrome V8 engine
4. ECMAScript 2015 spec functions help write lean code (like array functions, map and reduce functions)


###NOTE:
1. The command `node index.js < input.txt` might fail due to TTY error on git bash on
   WINDOWS based systems due to a bug on NodeJS side
   Ref: https://github.com/nodejs/node/issues/3006
   This works perfectly and has been tested on Linux based systems
