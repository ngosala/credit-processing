const luhn = function(cardNum) {
  // Define reducer to calculate sum of required array
  const reducer = (accumulator, currentVal) => (accumulator + currentVal);

  // split to digits and for every digit, multiply by 2 for every second index
  let transformArr = cardNum.split('').map(function(digit, idx, numArr) {
    digit = (idx % 2) ? digit : digit * 2;

    // If digit > 9, then subtract 9 from it
    if (digit > 9)
      return parseInt(digit - 9);
    else
      return parseInt(digit);
  });

  // Calculate the checksum
  let checkSum = transformArr.reduce(reducer);

  // If checksum mod 10 is 0, then card is valid
  return (checkSum % 10 === 0) ? true : false;
}

module.exports = luhn;